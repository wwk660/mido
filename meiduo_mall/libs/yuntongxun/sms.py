from ronglian_sms_sdk import SmsSDK
accId = '8a216da880d67afb018147c11ff31362'  # 开发者主账号 中的 ACCOUNT SID
accToken = '7a514a70e72c4c7aa05c9be24933d71c' # 开发者主账号 中的 AUTH TOKEN
appId = '8a216da880d67afb018147c120fd1369'  # 开发者主账号 中的 AppID

def send_message(phone, datas):
	sdk = SmsSDK(accId, accToken, appId)
	tid = '1' # 测试模板id为: 1. 内容为: 【云通讯】您的验证码是{1}，请于{2}分钟内正确输入。
	# mobile = '133034xxxx'
	# datas = ('666777', '5') # 模板中的参数按照位置传递 （验证码，有效时间）
	resp = sdk.sendMessage(tid, phone, datas)
	return resp

if __name__ == '__main__':
    send_resp = send_message(18810355622, (8888, "5"))