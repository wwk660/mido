# 生产者 -- 任务，函数
# 1. 这个函数 必须要让celery的实例的 task装饰器 装饰
# 2. 需要celery 自动检测指定包的任务

from libs.yuntongxun.sms import send_message
from celery_tasks.main import app

@app.task
def celery_send_sms_code(mobile,code):
    send_message(mobile, (code, "5"))

