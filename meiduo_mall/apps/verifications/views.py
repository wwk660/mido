from django.shortcuts import render
from django.http import JsonResponse,HttpResponse

# Create your views here.
"""
前端
     拼接一个 url 。然后给 img 。img会发起请求
     url=http://mp-meiduo-python.itheima.net/image_codes/ca732354-08d0-416d-823b-14b1d77746d2/

     url=http://ip:port/image_codes/uuid/
     this.image_code_url = this.host + "/image_codes/" + this.image_code_id + "/";

后端
    请求              接收路由中的 uuid
    业务逻辑          生成图片验证码和图片二进制。通过redis把图片验证码保存起来
    响应              返回图片二进制

    路由：     GET     image_codes/uuid/
    步骤：         
            1. 接收路由中的 uuid
            2. 生成图片验证码和图片二进制
            3. 通过redis把图片验证码保存起来
            4. 返回图片二进制
"""
from django.http import HttpResponse
from django.views import View


class ImageCodeView(View):
    def get(self, request, uuid):
        # print(f"uuid:{uuid}")
        # 1. 接收路由中的 uuid
        # 2. 生成图片验证码和图片二进制
        from libs.captcha.captcha import captcha
        # text 是图片验证码的内容 例如： xyzz
        # image 是图片二进制
        text, image = captcha.generate_captcha()

        # 3. 通过redis把图片验证码保存起来
        # 3.1 进行redis的连接
        from django_redis import get_redis_connection
        redis_cli = get_redis_connection('code')
        # 3.2 指令操作
        # name, time, value
        redis_cli.setex(uuid, 100, text)

        # 4. 返回图片二进制
        # 因为图片是二进制 我们不能返回JSON数据
        # content_type=响应体数据类型
        # content_type 的语法形式是： 大类/小类
        # content_type (MIME类型)
        # 图片： image/jpeg , image/gif, image/png
        return HttpResponse(image, content_type='image/jpeg')


#################################################################################

"""
1.注册
我们提供免费开发测试，【免费开发测试前，请先 注册 成为平台用户】。
2.绑定测试号
免费开发测试需要在"控制台—管理—号码管理—测试号码"绑定 测试号码 。
3.开发测试
开发测试过程请参考 短信业务接口 及 Demo示例 / sdk参考（新版）示例。Java环境安装请参考"新版sdk"。
4.免费开发测试注意事项
    4.1.免费开发测试需要使用到"控制台首页"，开发者主账户相关信息，如主账号、应用ID等。
    4.2.免费开发测试使用的模板ID为1，具体内容：【云通讯】您的验证码是{1}，请于{2}分钟内正确输入。其中{1}和{2}为短信模板参数。
    4.3.测试成功后，即可申请短信模板并 正式使用 。
"""

"""
发送验证码流程
前端
            当用户输入完 手机号，图片验证码之后，前端发送一个axios请求
            sms_codes/18310820644/?image_code=knse&image_code_id=b7ef98bb-161b-437a-9af7-f434bb050643

'/sms_codes/' + this.mobile + '/' + '?image_code=' + this.image_code
                + '&image_code_id=' + this.image_code_id
后端

    请求：     接收请求，获取请求参数（路由有手机号， 用户的图片验证码和UUID在查询字符串中）
    业务逻辑：  验证参数， 验证图片验证码， 生成短信验证码，保存短信验证码，发送短信验证码
    响应：     返回响应
            {‘code’:0,'errmsg':'ok'}


    路由：     GET     sms_codes/18310820644/?image_code=knse&image_code_id=b7ef98bb-161b-437a-9af7-f434bb050643

    步骤：
            1. 获取请求参数
            2. 验证参数
            3. 验证图片验证码
            4. 生成短信验证码
            5. 保存短信验证码
            6. 发送短信验证码
            7. 返回响应

需求 --》 思路 --》 步骤 --》 代码

debug 模式 就是调试模式 （小虫子）
debug + 断点配合使用 这个我们看到程序执行的过程

添加断点 在函数体的第一行添加！！！！！
"""

import random
import logging
logger = logging.getLogger('django')
class SmsCodeView(View):
    def get(self, request, mobile):
        # 1.获取请求参数
        image_code = request.GET.get('image_code')
        uuid = request.GET.get('image_code_id')

        #2.校验参数
        if not all([image_code,uuid]):
             return JsonResponse({'code':400,'errmsg':'参数不全'})

        # 3. 验证图片验证码
        #3..1创建连接到redis的对象
        from django_redis import get_redis_connection
        redis_conn = get_redis_connection('code')
        #3.2 提取图形验证码
        image_code_server = redis_conn.get(uuid)
        if image_code_server is None:
            #图形验证码过期或者不存在
            return JsonResponse({'code': 400, 'errmsg': '图形验证码失效'})
        #3.3 删除图形验证码 避免恶意测试图形验证码
        try:
            redis_conn.delete(uuid)
        except Exception as e:
            logger.error(e)

        #3.4 对比图形验证码
        image_code_server=image_code_server.decode()#bytes转字符串
        if image_code_server.lower() != image_code.lower(): #统一转小写后比较
            return JsonResponse({'code': 400, 'errmsg': '输入图形验证码有误'})

        #3.5  避免频繁发送短信验证码逻辑实现
        #提取发送短信的标记，看看有没有
        send_flag = redis_conn.get('send_glag_%s'%mobile)
        if send_flag is not None:
            return JsonResponse({'code':400,'errmsg':'不要频繁发送短信'})


        #4生成短信验证码
        #4.1 速记生成6位数字验证码
        sms_code = '%06d' %random.randint(0,999999)
        print(sms_code)
        logger.info(sms_code)
        #4.2 保存短信验证码
        # redis_conn.setex(mobile,300,sms_code)
        # # # 添加一个发送标记.有效期 60秒 内容是什么都可以
        # redis_conn.setex('send_flag_%s'%mobile,60,1)

        #通过管道实现4.2
        #管道3步
        # （1）新建一个管道
        pipeline = redis_conn.pipeline()
        # (2)管道收集命令
        #保存短信验证码
        pipeline.setex(mobile,300,sms_code)
         # 添加一个发送标记.有效期 60秒 内容是什么都可以
        pipeline.setex('send_flag_%s'%mobile,60,1)
        # (3)管道执行指令
        pipeline.execute()


        #4.3 发送短信验证码
        # from libs.yuntongxun.sms import send_message
        # send_message(mobile, (sms_code, "5"))

        #采用celery发送短信验证码
        from celery_tasks.sms.tasks import celery_send_sms_code
        celery_send_sms_code(mobile,sms_code)


        #5响应结果
        return JsonResponse({'code': 0, 'errmsg': '发送短信成功'})

