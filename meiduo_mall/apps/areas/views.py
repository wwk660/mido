from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
from django.views import View
from apps.areas.models import Area
from django.core.cache import cache
import logging

logger = logging.getLogger('django')

"""
需求：
    获取省份信息
前端：
    当页面加载的时候，会发送axios请求，来获取 省份信息
后端：
    请求：         不需要请求参数
    业务逻辑：       查询省份信息
    响应：         JSON

    路由：         areas/
    步骤：
        1.查询省份信息
        2.将对象转换为字典数据
        3.返回响应
"""
class AreaView(View):
    #提供省份数据
    def get(self,request):
        #先查询缓存数据
        province_list=cache.get('province')  #from django.core.cache import cache 相当于直接引入CACHES配置项中的’DEFAULT’项
        #如果没有缓存，则查询数据库，并缓存数据
        if province_list is None:
            try:
                #1.查找省份数据
                provinces = Area.objects.filter(parent__isnull=True)
                #2.将对象转化为字典数据  序列化省级数据
                province_list=[]
                for province in provinces:
                    province_list.append(
                        {
                            'id':province.id,
                            'name':province.name
                        }
                    )
                    #保存缓存数据
                    #cache.set(key,value,expire)
                    cache.set('province',province_list,24*3600)
            except Exception as e:
                logger.error(e)
                return JsonResponse({'code':400,'errmsg':'省份数据错误'})
        #响应省份数据
        return JsonResponse({'code':0,'errmsg':'OK','province_list':province_list})

"""
需求：
    获取市、区县信息
前端：
    当页面修改省、市的时候，会发送axios请求，来获取 下一级的信息
后端：
    请求：         要传递省份id、市的id
    业务逻辑：       根据id查询信息，将查询结果集转换为字典列表
    响应：         JSON

    路由：         areas/id/
    步骤：
        1.获取省份id、市的id,查询信息
        2.将对象转换为字典数据
        3.返回响应
"""
class SubAreaView(View):
    def get(self,request,id):
        #1.先获取缓存数据
        sub_list =cache.get('city:%s'%id)
        if sub_list is None:
            try:
                parent_model = Area.objects.get(id=id)#查询市或区的父级
                sub_model_list=parent_model.subs.all()

                #序列化市或区数据
                sub_list=[]
                for sub_model in sub_model_list:
                    sub_list.append({
                        "id":sub_model.id,
                        "name":sub_model.name
                    })
                #缓存数据
                cache.set('city:%s'%id,sub_list,24*3600)
            except Exception as e:
                logger.error(e)
                return JsonResponse({'code':400,'errmsg':'城市或区数据错误'})

            #返回响应
        return JsonResponse({'code':0,'errmsg':'ok','sub_data':{'subs':sub_list}})







