from django.db import models

# Create your models here.
from django.db import models


class Area(models.Model):
    """省市区"""
    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL,
                               related_name='subs',
                               null=True, blank=True, verbose_name='上级行政区划')

     #一对多：一的实例名. 反向查询的模型类名小写_set.all()
    #查询书籍为1的所有人物信息
      # book=BookInfo.objects.get(id=1)
       # book.peopleinfo_set.all()
    # subs = [Area,Area,Area]
    #  related_name 关联的模型的名字
    # 默认是 关联模型类名小写_set     area_set
    # 我们可以通过 related_name 修改默认是名字，现在就改为了 subs

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '省市区'
        verbose_name_plural = '省市区'

    def __str__(self):
        return self.name


"""
省市区


id          name            parent_id

10000       河北省             NULL

10100       保定市             10000
10200       石家庄市            10000
10300       唐山市             10000


10101       雄县              10100
10102       安新县             10100



查询省份信息
 select * from tb_areas where parent_id is NULL;

 Area.objects.filter(parent=None)
 Area.objects.filter(parent__isnull=True)
 Area.objects.filter(parent_id__isnull=True)


查询市的信息
    Area.objects.filter(parent_id=130000)
    Area.objects.filter(parent=130000)

    >>> province=Area.objects.get(id=130000)  #省
    >>> province.subs.all()                   #市


查询区县的信息
select * from tb_areas where parent_id=130600;

    Area.objects.filter(parent_id=130600)
    Area.objects.filter(parent=130600)

    >>> city=Area.objects.get(id=130600)   #市
    >>> city.subs.all()                    #区县       类名.objects.all()：获取所有数据



买主模型以及水果模型，一个买主对应多个水果模型，简单的一对多模型：
class Buyer(models.Model)：
    name = models.CharField(verbose_name='买主名', max_length=10) 
    Alipay_id = models.CharField(verbose_name='支付宝账号') 
    age = models.IntegerField(verbose_name='买主年龄'，blank = True)
    
class Fruit(models.Model): 
    buyer = models.ForeignKey(Buyer, related_name='buyer_fruit') 
    fruit_name = models.CharField(verbose_name='水果名', max_length=10) 
    weight = models.FloatField(verbose_name='水果重量') 
    
通常，我们要查询买主买了哪些水果,首先要根据条件找到买主信息，然后根据买主信息找到买主所购买的水果，本例中如下：
#首先获得水果模型中外键指向的表中对象：
 
buyer = Buyer.objects.filter(age = 100).first()
 
#然后通过‘_set’方法获得子表中的数据：
 
fruits = buyer.fruit_set.all() 
 
# django 默认每个主表的对象都有一个是外键的属性，可以通过它来查询到所有属于主表的子表的信息。 
# 这个属性的名称默认是以子表的名称小写加上_set()来表示,这里我们的主表是buyer,字表是fruit,所以主表外键的属性就是fruit_set

上面的fruit_set是django为对象buyer默认创建的外键的属性，个人建议采用自定义的方式定义主表的外键，这样使用时更熟悉一些吧！
而related_name就实现这个功能，在字表中定义外键时，增加related_name字段指定这个字表在主表中对应的外键属性， 如下：

class Fruit(models.Model): 
    buyer = models.ForeignKey(Buyer, related_name='buyer_fruit') 

主表:buyer
子表：fruit
子表在主表中对应的外键属性：related_name='buyer_fruit'

那么我们就可以通过自定义外键的方式找到需要的信息了：
#首先获得水果模型中外键指向的表中对象：
 
buyer = Buyer.objects.filter(age = 100).first()
 
#然后通过子表中自定义的外键获取子表的所有信息：
 
fruits = buyer.buyer_fruit.all() 
"""



